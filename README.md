![Docker Cloud Automated build](https://img.shields.io/docker/cloud/automated/dftechs/ubuntu-dev?style=for-the-badge)
![Docker Cloud Build Status](https://img.shields.io/docker/cloud/build/dftechs/ubuntu-dev?style=for-the-badge)

![Docker Image Size (tag)](https://img.shields.io/docker/image-size/dftechs/ubuntu-dev/latest?style=for-the-badge)
